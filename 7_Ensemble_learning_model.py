import pandas as pd
import numpy as np
import pickle

from sklearn.model_selection import train_test_split
from sklearn.model_selection import GridSearchCV
from sklearn.neighbors import KNeighborsClassifier
from sklearn.ensemble import RandomForestClassifier
from sklearn.linear_model import LogisticRegression
from sklearn.model_selection import cross_val_score
from sklearn.ensemble import VotingClassifier

df = pd.read_csv('data/diabetes_data.csv')

df.head()

# split data into inputs and targets
X = df.drop(columns=['diabetes'])
y = df['diabetes']

# split data into train and test sets. 30% por testing. 70% for training.
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.3, stratify=y)

# K-NEAREST NEIGHBORS(kNN)
# Tomamos un punto de los datos y buscamos los 'k' más cercanos.
# si k=5, si 3 puntos son verdes y 2 rojos, los datos de ese punto en cuestión son verder, porque verde es la mayoría.
# Primero creamos un clasificador k-NN. luego creamos un diccionarion para guardar todos los valores que
# probaremos con 'n_neighbors'.
knn = KNeighborsClassifier()
params_knn = {'n_neighbors': np.arange(1, 25)}

# Crearemos nuestra grilla de buqueada para probar todos los valores paraa 'n_neighbors'
knn_gs = GridSearchCV(knn, params_knn, cv=5)
# Ajustamos el modelo a los datos de entrenamiento.
knn_gs.fit(X_train, y_train)

# save best model
knn_best = knn_gs.best_estimator_
# check best n_neigbors value
print('K-NN score: ', knn_gs.best_params_)

# RANDOM FOREST
# create a new random forest classifier
rf = RandomForestClassifier()
# create a dictionary of all values we want to test for n_estimators
params_rf = {'n_estimators': [50, 100, 200]}
# use gridsearch to test all values for n_estimators
rf_gs = GridSearchCV(rf, params_rf, cv=5)
# fit model to training data
rf_gs.fit(X_train, y_train)

# save best model
rf_best = rf_gs.best_estimator_
# check best n_estimators value
print('Random Forest score: ', rf_gs.best_params_)

# LOGISTIC REGRESSION
# create a new logistic regression model
log_reg = LogisticRegression(solver='lbfgs', multi_class='auto', max_iter=500)
# fit the model to the training data
log_reg.fit(X_train, y_train)

scores = cross_val_score(log_reg, X, y, cv=10, scoring='r2')
print('Logistic regression score: {}'.format(scores))

# test the three models with the test data and print their accuracy scores
print('knn: {}'.format(knn_best.score(X_test, y_test)))
print('rf: {}'.format(rf_best.score(X_test, y_test)))
print('log_reg: {}'.format(log_reg.score(X_test, y_test)))

# Votando por el clasificador
# create a dictionary of our models
estimators = [('knn', knn_best), ('rf', rf_best), ('log_reg', log_reg)]
# create our voting classifier, inputting our models
ensemble = VotingClassifier(estimators, voting='hard')

# Ajustamos el modelo a los datos de entrenamiento
ensemble.fit(X_train, y_train)
# Probamos el modelo en los datos de prueba.
ensamble_score = ensemble.score(X_test, y_test)
# Asombrosamente nuestro modelo ensamblado mejora la predicciòn de los datos.

print('El score del modelo ensamblado: ', ensamble_score)

# SERIALIZANDO NUESTRO MODELO ENSAMBLADO A UN ARCHIVO

# serializing our model to a file called ensamble_model.pkl

pickle_out = open("dict.pickle","wb")
pickle.dump(ensemble, pickle_out)
pickle_out.close()

#pickle.dump(ensemble, open("ensamble_model.pkl", "wb"))

print('Se ha serializado el modelo.')
