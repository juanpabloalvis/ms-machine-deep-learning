import matplotlib.pyplot as plt
from keras.datasets import mnist
from keras import models
from keras import layers
from keras.utils import to_categorical

(train_images, train_labels), (test_images, test_labels) = mnist.load_data()

print('Train images: ', train_images.shape)
print('Train labels: ', train_labels)
print('Length train labels: ', len(train_labels))

print('Test Images: ', test_images.shape)
print('Length test labels: ', len(test_labels))

print('train_images[0]: ', train_images[0])

# creamos el modelo
neural_network = models.Sequential()
neural_network.add(layers.Dense(16, activation='relu', input_shape=(28 * 28,)))
neural_network.add(layers.Dense(16, activation='relu'))
neural_network.add(layers.Dense(10, activation='softmax'))

# compilamos la red
neural_network.compile(optimizer='rmsprop',
                       loss='categorical_crossentropy',
                       metrics=['accuracy'])
# Se cambia de dos a una sola dimension
print(train_images.shape)
train_images = train_images.reshape((60000, 28 * 28))
print(train_images.shape)
train_images = train_images.astype('float32') / 255

test_images = test_images.reshape((10000, 28 * 28))
test_images = test_images.astype('float32') / 255

print(test_images[0])

# Convertimos las etiquetas del objetivo a categoricas (0, 1, 2, ..., 9 ---> en categoricamente)
print('Train_labels: ', train_labels[0])
train_labels = to_categorical(train_labels)
print('Train_labels: ', train_labels[0])
test_labels = to_categorical(test_labels)

# Cuantas iteraciones ocurren?
# Muestra de entrenamiento =  60000
# En cada epoca la red debería ver 60000
# pero nuestra entrada es en lotes de 128

# para cada epoc hacemos: 60000/128 = 468.75 o 468 iteraciones.
# 1 epoca = 469 iteraciones.
# 5 epocas = 2345.
# En total la red debería haber hecho 243 actualizaciones al gradiante( actualiza los pesos y el bias)

history = neural_network.fit(train_images, train_labels, epochs=5, batch_size=128,
                             validation_data=(test_images, test_labels))

print(neural_network.layers)
history_dict = history.history
print('history_dict: ', history_dict)
print('history_dict.keys: ', history_dict.keys())

acc = history.history['acc']
print('accurate model: ', acc)

val_acc = history.history['val_acc']
print('validation accurate: ', val_acc)

loss = history.history['loss']
val_loss = history.history['val_loss']
loss_values = history_dict['loss']
val_loss_values = history_dict['val_loss']

epochs = range(1, len(acc) + 1)

plt.plot(epochs, loss_values, 'bo', label='Training loss')
plt.plot(epochs, val_loss_values, 'b', label='Validation loss')
plt.title('Training and validation loss')
plt.xlabel('Epochs')
plt.ylabel('Loss')
plt.legend()

plt.show()

plt.clf()
acc_values = history_dict['acc']
val_acc_values = history_dict['val_acc']

plt.plot(epochs, acc, 'bo', label='Training acc')
plt.plot(epochs, val_acc, 'b', label='Validation acc')
plt.title('Training and validation accuracy')
plt.xlabel('Epochs')
plt.ylabel('Accuracy')
plt.show()
