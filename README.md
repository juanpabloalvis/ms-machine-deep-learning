
####Master Machine Learning and Deep Learning

Librerias: pip install ipython numpy scipy scikit-learn keyboard matplotlib keras fix-yahoo-finance pandas_datareader 
pip install --upgrade tensorflow


Problemas de aprendizaje:

* Regresión(Salida continua): Precios de acciones, precios de casas
* Clasificación(Valores discretos): Es spam o no. Quien ganará el partido del domingo.


####Conceptos
- *Funcionde costo(Loss function):* Es minimizar el error respecto a la hipótesis. En un mundo ideal quisieramos que fuera cero. El objetivo es reducir este error.
- Etiquetas de datos: Una etiqueta es algo verdadero que tratamos de predecir. Es una variable objetivo.
- Caraterísticas: Area, Ubicación, tipo de construcción, distancia, # cuartos,
- Muestra: Es un registro (Es una etiqueta y la variable objetivo)  
- Modelo: Lo podemos conocer o no. Pero nos permitirá predecir el precio de las acciones.
- Algoritmo: No sabemos que algonritmos utilizar.
Ejemplo lineal:
- Características: <img src="http://latex.codecogs.com/svg.latex?x_{1}, x_{2}, x_{3} ... x_{n}" />
- Pesos:  <img src="http://latex.codecogs.com/svg.latex?W_{1}, W_{2}, W_{3} ...W_{n}" />
- Hipótesis: <img src="http://latex.codecogs.com/svg.latex?h(x)= W_{1} x_{1} + W_{2} x_{2} + W_{3}x_{3} + ... + W_{n}x_{n} + C" />
- Sobrejuste(overfit)-hight variance: Ees cuando un modelo ha aprendido incluyendo los errores de la muestra. Es cuando nuestro modelo se ha sobreentrando al punto que predice la mayoría de casos solo del modelo de entrenamiento, sin  embargo está tan atado al modelo, que podría predecir incorrectamente nuevos datos. 
- Subjuste(underfit)-hight bias: Es cuando un modelo no está suficientemente entrenado y no puede aprender de los datos, está altamente sesgado.
Buscamos un modelo que no generalize ni overfit ni underfit, que esté balanceado.
Subajuste significa que el modelo es muy simple, necesita más características. Sobreajuste significa que el modelo es muy complejo, se puede simplificar utilizando menos características y regularización.
- Regularización: Significa reducir la comlpegidad del modelo. La manera más simple es agregar penalización a la funcion de costo a la proporcion del tamaó de las entradas en el modelo.
    - Weight Regularization: Penaliza el modelo durante el entrenamiento basado en la magnitud de los pesos.
    - Hay otros metetodos de regularización que son:
        - Activity Regularization: Penaliza el modelo durante el entrenamiento base en la magnitud de las activaciones.
        - Weight Constrain: Restring la magnitud de pesos que estaran dentro delun rango o siguiendo un limite.
        - Dropout: Probabilisticamente remueve entradas durante el entrenamiento.
        - Noise: Añade ruido estadístico a las entradas durante el entrenamiento.
        - Early Stopping: Monitorea el desempeño del modelo sobre una validacion y ajusta y detiene el entrenamiento cuando el desempeño se degrada.
        
           
El objetivo es hallar cada uno de los pesos de la hipótesis.

## 1) Regresion lineal
#####Pasos
1) Obtener datos etiquetados.
2) Separar datos entre datos de prueba  (30%) y datos de entrenamiento(70%).
    Hay un issue al hacer esta separación. Su los datos estan ordenados, hay problema. 
    Se debe hacer "Cross validation", que consiste en separar con el remanente(70%) y se itera 4 o 5 veces por cada 70% restante. Pora cada iteración, sacamos un "score" que luego promediamos por el número de iteraciones". 
3) Utilizamos los datos de entrenamiento para construir el modelo.
4) Una vez construido el modelo, obtenemos la precición del modelo utilizando datos de prueba. (60% no es bueno)
5) Afinar el modelo.
6) Predecir en producción.

Calidad de la elección de características
- Buenas características, ayudan an discriminar
- Deben agregar valor al resultado final
- Sin buenas características, los algoritmos de ML son inutiles

Estadística
- Media, es el promedio de todos los números de una muestra
- Mediana, se encuentra al ordenar un conjunto de datos de menor a mayor y encontrar el valor que se ubica en el medio. Si el conjunto de datos es par, se halla la media de los dos datos de la mitad.
- SD Desviación estándar, se representa por la letra signa σ o la letra s, es una medida que se utiliza para cuantificar la cantidad de variación o dispersión de un conjunto de valores

**Regresión lineal**
Es un modelo lineal que asume que hay una realación lineal entre las variables de entrada(x) y una sola salida (y). Mas específicamente la salida h0(x) puede ser calculada desde una combinación de variables de entrada (x):
 
 <img src="http://latex.codecogs.com/svg.latex?h(x)= W_{1} x_{1} + W_{2} x_{2} + W_{3}x_{3} + ... + W_{n}x_{n} + C" />
 
 
 
No solo es una linea. Cuando hay mayores dimensiones tenemos mas de una estrada (x) la linea es llamada un plano o hiper-plano.
* Error absoluto medio(Mean absolute error-MAE-). Es la suma de las diferencias absolutas entre las predicciones y los valores actuales.
Nos da una idea de que tan malas fueron las predicciones.
* Error cuadrático medio(ECM) -mean squared error (MSE) or mean squared deviation (MSD)-. Es como el error absoluto medio, que nos brinda una idea de la magnitud del error. Es una funcion de riesgo, correspondiente al valor esperado de la pérdida del error al cuadrado o la pérdida cuadrática.
* Raíz del error cuadrático medio(o raís de la desviación cuadrática media). Toma los valores del error cuadrático medio y lo convierte de regreso a unidades de la variable de salida original.
* R^2 o (R cuadrado). Es una métrica que brinda indicación de que tan bien cumple un conjunto de predicciones a los valores actuales. Es un valor entre 0 y 1, 0 cuando no cumple y 1 cumple perfectamente.
    
## 2) Clasificación

- Cómo se mide la clasificación
- La precisión no es suficiente
- Un algoritmo de prueba de cancer, puede decir siempre que la prueba de cancer es negativa. Esto daría una precición del 90%, porque quienes tienen cancer es del 3%, a pesar que 90% es un 'buen' score.

#####Conceptos
Verdaderos positivos VP (TP)
Verdaderos negativos VN (TN)
Falsos negativosF(FN)
Falsos Positivos(FP)

**Análisis de sensibilidad o Recall:** Es la capacidad de nuestro modelo para identificar los casos positivos, los casos realmente enfermos; proporción de enfermos correctamente identificados.
    La sensibilidad del modelo es = TP/(TP+FN)
    
**Análisis de especificidad:** Es la capacidad de nuestro modelo para identificar los casos negativos, que realmente estan sanos; proporción de sanos correctamente identificados. Se detecta la ausencia de la enfermedad.
    La especificidad del modelo es = TN/(TN+FP)
    
**Precisión o positive predictive value(PPV):** Es la porcion de identificaciones de positivos que fueron correctos.    
    Precisión = TP /(TP +FP)
![Image https://en.wikipedia.org/wiki/Sensitivity_and_specificity](https://upload.wikimedia.org/wikipedia/commons/thumb/3/3d/Sensibilidad_y_especificidad.svg/350px-Sensibilidad_y_especificidad.svg.png)

Rango normal:

Umbral
![http://www.drcoplan.com](http://www.drcoplan.com/wp-content/uploads/2013/08/chart.png)

Umbral 
![http://www.drcoplan.com](http://www.drcoplan.com/wp-content/uploads/2013/08/chart-two.png)

Especificidad máxima:

![http://www.drcoplan.com](http://www.drcoplan.com/wp-content/uploads/2013/08/graph-three.png)

Sensibilidad máxima
![http://www.drcoplan.com](http://www.drcoplan.com/wp-content/uploads/2013/09/pic-d.png)

La relacion entre sesibilidad y especificidad es inversamente proporcional, a mayor especificidad, menor sensibilidad.
Sensibilidad es otro nombre de recall.
Especificidad no es igual a precision 
Precisión son los valores positivos correctamente clasificados

**ROC**

Es una representación grafica de la sensibilidad frente a la especificidad para un sistema clasificador binario, según se varía el umbral.
 
![image Wiki](https://upload.wikimedia.org/wikipedia/commons/3/36/ROC_space-2.png)

AUC Area under the curve ROC = este indicador nos da una ideda que tan bien se desempeña el modelo. Tambien nos indica que tan bien separadas están las probabilidades posistivas de las negativas.
https://medium.com/greyatom/lets-learn-about-auc-roc-curve-4a94b4d88152

**Matriz de confución**

Tambien es conocida como la matriz de error. Es una tabla que permite la visualización de del desempeño de un algoritmo(modelo) comunmente utilizado en un problema de aprendizaje supervisado. En un problema de aprendizaje no supervisado, se llama matriz de coincidencia.
Las columnas representan el numero de predicciones de cada clase, y cada fiila representa los valores reales de cada clase. Permite identificar visualmente si el modelo está haciendo las predicciones correctamente como los errores.
Las muestras deben ser represntativas para que no haya sesgos. 

![image Wiki](/data/MatrizConfusion.PNG)


## 3) k vecinos más próximos (KNN )
Es un método utilizado para regresión y clasificación supervisado. Una métrica utlilizada para hallar a los puntos(vecinos) más cercanos en variables continuas, es la distancia euclidiana. Para variables discretas se utiliza la métrica de superpesoción. También se puede utilizar coeficientes de correlación como Pearson y Spearman. En ML la distancia es aprendida con algoritmos especializados como Large Margin Nearest Neighbor o Neighbourhood components analysis.
- KNN hace predicciones utilizando datos de entrenamiento directamente. 
- Las predicciones para una nueva instancia son hechas a travez de la búsqueda en el conjunto completo de entrenamiento para las instancias *k* más similares.
- Para regresión podría ser la media de la variable de salida, en clasificación podría ser la moda (el valor más común de la clase).
- Para detriminar cual de las instancias in el conjunto de entrenamiento es mas similar, se mide puede utiilizar la distancia Euclidea. 
La formula de la distancia euclidea es: ![image Wiki](https://wikimedia.org/api/rest_v1/media/math/render/svg/795b967db2917cdde7c2da2d1ee327eb673276c0)

**Regresión:**, la salida es el valor de la propiedad del objeto. Se halla identificando los vecinos que están más cerca de la muestra y se halla la media. 

**Clasificación:** la salida es un miembro de la clase. Se identifican los vecinos más cercanos, y se halla el valor más común.  

¿Cómo decidir el valor correcto de *k* ?
Este es el gran desafío de los problemas de ML. Para el problema actual vamos a utilizar cross validation y calculamos el mejor score. Utilizamos cross validation y una medida de precisión para el valor de *k* 
El valor decisivo de *K* es la optimización del HyperParameter 
Para problemas de clasificación k podría ser impar

TODO revisar como funcionan las 'epocas'

##### CNN Redes neuronales convulcionales
https://towardsdatascience.com/build-your-own-convolution-neural-network-in-5-mins-4217c2cf964f?gi=9197dd4e0586

http://www.diegocalvo.es/red-neuronal-convolucional/ 
Estas selección de características comienzan desde las de más bajo nivel hasta alto nivelo
Las CNN pueden tener multiples filtros. Estos filtros Se crean automaticamente. Estos filtros(kernels) pueden ser lineas verticales, border horizontales, lineas horizaontales, curvas y multiples formas.  