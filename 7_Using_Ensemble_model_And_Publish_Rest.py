import flask
import pandas as pd
import pickle
from sklearn.model_selection import train_test_split
from flask import request
import connexion

# ***START DATA TO PREDICT***
# IN THIS CASE I HAVE USED THE SAME DB, BUT SHOULD BE USE WITH NEW DATE
df = pd.read_csv('data/diabetes_data.csv')
X = df.drop(columns=['diabetes'])
y = df['diabetes']
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.3, stratify=y)
# ***END DATA TO PREDICT***

# loading a model from a file called ensamble_model.pkl
model = pickle.load(open("ensamble_model.pkl", "rb"))

print('Las predicciones para el modelo ensamblado son: ', model.predict(X_test)[0:5])

# code which helps initialize our server
# app = flask.Flask(__name__)
app = connexion.App(__name__, specification_dir='./')


@app.route('/', methods=['GET'])
def home():
    print('Fucking shit')
    return '''<h1>Api de pruebas para el modelo predictivo</h1>
<p>Un prototipo de un API que utiliza ML y Deep learnig. Ejemplo de un modelo ensamblado.</p>'''


# defining a /hello route for only post requests
@app.route('/hello', methods=['GET'])
def index():
    # grabs the data tagged as 'name'
    # name = request.get_json()['name']

    # sending a hello back to the requester
    return "Hello " + "There"


@app.route('/predict', methods=['POST'])
def predict():
    # grabbing a set of wine features from the request's body
    feature_array = request.get_json()['feature_array']

    # our model rates the wine based on the input array
    prediction = model.predict([feature_array]).tolist()

    # preparing a response object and storing the model's predictions
    response = {}
    response['predictions'] = prediction

    # sending our response object back as json
    return flask.jsonify(response)


# Read the swagger.yml file to configure the endpoints
# app.add_api('swagger.yml')

# If we're running in stand alone mode, run the application
if __name__ == '__main__':
    app.run(host='127.0.0.1', port=5000, debug=True)
