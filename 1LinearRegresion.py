# conventional way to import pandas
import pandas as pd
import numpy as np
from sklearn import linear_model, metrics
from sklearn.linear_model import LinearRegression
from sklearn.model_selection import train_test_split
from sklearn.model_selection import cross_val_score

# read CSV file directly from a URL and save the results
# data = pd.read_csv('http://datasciencemastery.in/wp-contentf/uploads/2018/07/Advertising.csv', index_col=0)
# leemos datos desde un archivo
data = pd.read_csv('data/Advertising.csv', index_col=0)

# Mostramos los primeros 5 registros. Este dataset nos muestra el gasto en publicidad en millones de datos Tv, Radio y Periodico
data.head()

# 1) obtener datos etiquetados (deben coincidir con los nombres de las columnas)
features_cols = ['TV', 'radio', 'newspaper']
X = data[features_cols]
print(X.head())

y = data['sales']

# 2 Separar datos entre datos de prueba  (30%) y datos de entrenamiento(70%). hacer Cross validation. random_state, es la semilla utilizada para el generador de numero aleatorios.
# Split simple
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.10, random_state=1)
# Isntantiate
linreg = LinearRegression()
# 3.1 ajustamos(fit) el modelo al los datos de entrenamiento (aprender de los coeficientes)
linreg.fit(X_train, y_train)
# 3.1 Creamos el modelo
# LinearRegression(copy_X=True, fit_intercept=True, n_jobs=1, normalize=False)

# 4) Hacemos las predicciones sobre los datos de prueba:
y_predict = linreg.predict(X_test)

for mp, av in zip(y_predict, y_test):
    print('Model predicted Value = {}, Known Value = {}, diference = {}'
          .format(round(mp, 2), round(av, 2), round(abs(mp - av), 2)))
print('')
print('Mean Absolute Error = {}'.format(metrics.mean_absolute_error(y_test, y_predict)))
print('Mean Squared Error = {}'.format(metrics.mean_squared_error(y_test, y_predict)))
print('Root Mean Squared Error = {}'.format(np.sqrt(metrics.mean_squared_error(y_test, y_predict))))
print('R2 score = {}'.format(metrics.r2_score(y_test, y_predict)))

# Utilizando Cross Validation, el beneficio de validación cruzada es el desempeño de cada regresion lineal

lm = linear_model.LinearRegression()

# cv es el número de pliegues estratificados de validación cruzada, se podría especificar uno
scores = cross_val_score(lm, X, y, cv=10, scoring='r2')

print('------------Muestra cada uno de los 10 pliegues--------------')
print(scores)
print('The mean of R2 is: ', scores.mean())
scores = cross_val_score(lm, X, y, cv=10, scoring='neg_mean_absolute_error')
print('The mean of absolute error is: ', scores.mean())

""" 
 ¿Qué nos dicen los 'scores'?. En este ejercicio predecimos ventas.
 El modelo nos dice que hay un error medio absoluto 'z', este modelo
 predictivo nos dice que podemos esperar ese valor en ambas direcciones, hacia
 arriba o hacia abajo. Si el modelo precide un valor 'p' bajo, el modelo es bueno,
 si es muy alto, el modelo es malo. Por lo tanto, los scores nos dice que tan bueno es el modelo.
 Si hay mucha diferencia, Debemos encontrar un modelo que reduzca dicho error.
 El proposito de la validación cruzada es dar el sentido del score, el cual se refleja en el negocio
"""

# Llevando el modelo a producción.
# Tomamos todos los datos
linreg = LinearRegression()
linreg.fit(X, y)

df = pd.read_csv('http://datasciencemastery.in/wp-content/uploads/2018/07/Advertising_Data_For_Prediction.csv', index_col=0)
print(df)
input_data = df[features_cols]
y_predict = linreg.predict(input_data)

print("\n", y_predict)

