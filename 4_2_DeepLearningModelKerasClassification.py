import pandas as pd
import keras
from keras.utils import to_categorical

from keras.models import Sequential
from keras.layers import Dense
from keras.callbacks import EarlyStopping

# Datos de entrada
training_df = pd.read_csv('data/diabetes_data.csv')
print(training_df.head())

training_X = training_df.drop(columns='diabetes')
training_Y = to_categorical(training_df.diabetes)

print(training_Y)

model = Sequential()

n_cols = training_X.shape[1]
print(n_cols)

# Agregamos capas al modelo
# la activación softmax permite la saliad de suma hasta 1, así
# la salida puede ser interpretada como probabilidades. El modelo hará
# predicciones basadas en la opción que tenga mayor probabilidad.
# La última capa del modelo tiene dos nodos, uo para cada opcion: el paciente tiene o no diabetes.
# IMPORTANTE: La capa de entreda es 'input_shape' que se comunica con la primera capa oculta que es 250.
# Dos es la capa de salida porque son dos tipos de salida, que es una probabilidad de tiener o no diabetes
model.add(Dense(250, activation='relu', input_shape=(n_cols,)))
model.add(Dense(250, activation='relu'))
model.add(Dense(250, activation='relu'))
model.add(Dense(2, activation='softmax'))

# Compilamos el modelo
# Utilizaremos etropía categorica para la función de costo. Es la elección mas común para clasificación.
# un score bajo indica que el modelo se desempeña mejor. Para hacer las cosas faciles el
# interpreter, utilizaremos la métrica de precisión.

model.compile(optimizer='adam', loss='categorical_crossentropy', metrics=['accuracy'])

early_stopping_monitor = EarlyStopping(patience=3)

model.fit(training_X, training_Y, epochs=30, validation_split=0.2, callbacks=[early_stopping_monitor], verbose=1)

print('**************')
print(model.summary())

print('Aquí ya podríamos predecir con nuevos datos')



