import pandas as pd
import numpy as np
from sklearn.model_selection import train_test_split
from sklearn.model_selection import cross_val_score
from sklearn.model_selection import GridSearchCV
from sklearn.neighbors import KNeighborsClassifier
from sklearn.metrics import accuracy_score, recall_score, f1_score, roc_auc_score, make_scorer

# load dataset
# https://towardsdatascience.com/building-a-k-nearest-neighbors-k-nn-model-with-scikit-learn-51209555453a
df = pd.read_csv('data/diabetes_data.csv')

# check data has been read in properly
print(df.head())
# check number of rows and columns in dataset
print(df.shape)

# Creamos un dataframe con los datos de entrenamiento sin la columna que tratamos de predecir
X = df.drop(columns=['diabetes'])
# Guradamos la columna 'diabetes' en nuestra variable objetivo y
y = df['diabetes'].values

# Vemos los datos
print('-------------------Datos de entrenamiento-------------------')
print(X.head())
print('Los primeros 5 datos')
print(y[0:5])

# Separamos los datos entre entrenamiento y prueba
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=1, stratify=y)

# Creamos el clasificador KNN cont 3 vecinos. Para resumir, esto significa que si al menos 2 de los 3 puntos más
# cercanos a un nuevo punto de datos son pacientes sin diabetes, el nuevo punto de datos se
# etiquetará como "sin diabetes" y viceversa. Es decir, un nuevo punto de datos se etiqueta
# con la mayoría de los 3 puntos más cercanos. Aunque seleccionamos 3 puntos, mas adelante vamos a ver como
# seleccionar el numero de vecinos y como mejorar el desempeño.
knn = KNeighborsClassifier(n_neighbors=3)

# Ajustamos el clasificador con los datos de entrenamiento
knn.fit(X_train, y_train)

# Mostramos las primeras 5 predicciones en los datos de prueba y
print('Las primeros 5 predicciones son: ', knn.predict(X_test)[0:5])
# y miramos la predicción del modelo en el test completo. Esto lo hacemos utilizando la vundion score y
# le pasamos los datos de prueba y los objetivos
print('El Score del modelo es: ', knn.score(X_test, y_test),
      '. 66.88% no es tan malo, pero podemos mejorar el desempeño.')

'''
VALIDACION CRUZADA
 Hacemos k-fold validation, tal cual hicimos en la regresión lineal, en este caso La validación cruzada se produce
 cuando el conjunto de datos se divide aleatoriamente en grupos "k". Funciona de la siguiente manera: Unod de
 los grupos se utilzia como pruebas de entrenamiento y  el resto como entrenamiento.  El modelo se entrena en
 el conjunto de entrenamien to y se califica en el conjunto de prueba y se itetera hasta que cada gurpo se utiliza
 como conjunto de prueba.
 Por ejemplo para desdoblaje de 5 validaciones cruzadas, el conjunto se divide en 5 grupos. y el modelo puede
 ser entrenado y probado 5 veces separadas con cada grupo. Ej: https://en.wikipedia.org/wiki/Cross-validation_(statistics)#/media/File:K-fold_cross_validation_EN.jpg
'''
# Creamos un nuevo modelo KNN
knn_cv = KNeighborsClassifier(n_neighbors=3)

# Entrenamos el modelo con 5 validaciones cruzadas
cv_scores = cross_val_score(knn_cv, X, y, cv=5)

# Imprimimos la precision de cada iteración
print('Score (precision) utilizando validación cruzada: ', cv_scores)
print('Promedio de los cv_scores:{}'.format(np.mean(cv_scores)))

'''
SUPER AFINANDO EL MODELO CON GridSearchCV

La puesta a punto de los parámetros ocurre cuando se pasa por un proceso para encontrar los parámetros óptimos para 
que su modelo mejore la precisión. En nuestro caso, usaremos GridSearchCV para encontrar el valor óptimo para "n_neighbors".
Para esto establecemos un rango para los valores 'n_neighbors' con el fin de ver cuales valores funciones mejor en 
nuestro modelo, por medio de un array  de 1-24.
'''
# creamos un nuevo modelo knn2
knn2 = KNeighborsClassifier()

# Creamos una grilla con los valores que vamos a probar
param_grid = {'n_neighbors': np.arange(1, 25)}

# Utilizamos la grilla para probar todos los valores de los n_neighbors
knn_gscv = GridSearchCV(knn2, param_grid, cv=5)

# Ajustamos el modelo a los s
knn_gscv.fit(X, y)

# Después de ajustar podemos validar cuales valores de lo n_neighbors que probamos son mejores.
print('Estos son los mejores valores para n_neighbors', knn_gscv.best_params_)
print('Cuyo mejor desempeño es: ', knn_gscv.best_score_)  # Con esto hemos mejorado el modelo

print(pd.DataFrame(knn_gscv.cv_results_))



print('Promedio de los cv_scores:{}'.format(np.mean(cv_scores)))

