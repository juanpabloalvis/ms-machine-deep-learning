import numpy as np
from sklearn.datasets import make_circles
import matplotlib.pyplot as plt
import time
from IPython.display import clear_output

''' https://www.youtube.com/watch?v=W8AeOXa_FqU'''

n = 500
p = 2  # Numero de entradas

X, Y = make_circles(n_samples=n, factor=0.5, noise=0.05)

Y = Y[:, np.newaxis]

plt.scatter(X[Y[:, 0] == 0, 0], X[Y[:, 0] == 0, 1], c="skyblue")
plt.scatter(X[Y[:, 0] == 1, 0], X[Y[:, 0] == 1, 1], c="salmon")
plt.axis("equal")
plt.show()

''' En una red neuronal, lo mas importante es la capa.
Vamoa definir un objeto que sea una capa. En este caso no tentra lógica, 
pero podría tenerla '''


class neural_layer():
    # Numero de conexiones
    def __init__(self, n_connection, n_neur, act_f):
        self.act_f = act_f

        self.b = np.random.rand(1, n_neur) * 2 - 1
        self.W = np.random.rand(n_connection, n_neur) * 2 - 1


## FUNCIONES DE ACTIVACION
# Podemos definir dos funciones en una, la sigmoide, y su derivada.
sigm = (lambda x: 1 / (1 + np.e ** (-x)),  # Funcion sigmoide sigm[1](valor)
                lambda x: x * (1 - x))     # dx Funcion sigmoide. Se puede acceder así: sigm[1](valor)
relu = lambda x: np.maximum(0, x)
# prueba. Creamos un vector de 100 valores de -5 hasta 5 y lo graficacoms
_x = np.linspace(-5, 5, 100)

#plt.plot(_x, sigm[1](_x))
plt.plot(_x, relu(_x))
plt.show()

# ahora podemos crear varias capas a partir de la clase. (4 es el numeor de conexiones)
l0 = neural_layer(p, 4, sigm)
l1 = neural_layer(4, 8, sigm)


# ...

# El primer término corresponde al numero de entradas, y el útlimo parámetro es la salida que es 1.


def create_neuaral_network(topology, activation_function):
    neural_newtwork = []
    for i, layer in enumerate(topology[:-1]):  # Nos da el indice y el objeto
        neural_newtwork.append(neural_layer(topology[i], topology[i + 1], activation_function))
    return neural_newtwork


# Codigo encargado de entrenar la red
#topology = [p, 4, 8, 16, 8, 4, 1]
topology = [p, 4, 8, 1]
neural_net = create_neuaral_network(topology, sigm)

l2_costo = (lambda Yp, Yr: np.mean((Yp - Yr) ** 2),
            lambda Yp, Yr: (Yp - Yr))

def train(neural_net, X, Y, l2_costo, learningRate=0.5, train=True):
    out = [(None, X)]
    # Forward pass: tomamos el vector de entrada y lo pasamos por las capas
    # Arroba indica multiplicado matricilamente
    for l, layer in enumerate(neural_net):
        z = out[-1][1] @ neural_net[l].W + neural_net[l].b
        a = neural_net[l].act_f[0](z)  # esta seria la salida de la capa 1
        out.append((z, a))

    if train:
        # Backward pass
        deltas = []
        for lyr in reversed(range(0, len(neural_net))):
            z = out[lyr + 1][0]
            a = out[lyr + 1][1]
            # print(a.shape)
            if lyr == (len(neural_net) - 1):
                deltas.insert(0, l2_costo[1](a, Y) * neural_net[lyr].act_f[1](a))  # Calcular delta ultima capa
            else:
                # .T es la transpuesta
                deltas.insert(0, deltas[0] @ _W.T * neural_net[lyr].act_f[1](a))  # Calcular delta a capa previa

            _W = neural_net[lyr].W
            # Gradient Descent
            neural_net[lyr].b = neural_net[lyr].b - np.mean(deltas[0], axis=0, keepdims=True) * learningRate
            neural_net[lyr].W = neural_net[lyr].W - out[lyr][1].T @ deltas[0] * learningRate
    return out[-1][1]


train(neural_net, X, Y, l2_costo, 0.5)

neural_n = create_neuaral_network(topology, sigm)

loss = []

for i in range(100):

    # Entrenemos a la red!
    pY = train(neural_n, X, Y, l2_costo, learningRate=0.05)

    if i % 25 == 0:

        #print(pY)

        loss.append(l2_costo[0](pY, Y))

        res = 50

        _x0 = np.linspace(-1.5, 1.5, res)
        _x1 = np.linspace(-1.5, 1.5, res)

        _Y = np.zeros((res, res))

        for i0, x0 in enumerate(_x0):
            for i1, x1 in enumerate(_x1):
                _Y[i0, i1] = train(neural_n, np.array([[x0, x1]]), Y, l2_costo, train=False)[0][0]

        plt.pcolormesh(_x0, _x1, _Y, cmap="coolwarm")
        plt.axis("equal")

        plt.scatter(X[Y[:,0] == 0, 0], X[Y[:,0] == 0, 1], c="skyblue")
        plt.scatter(X[Y[:,0] == 1, 0], X[Y[:,0] == 1, 1], c="salmon")

        clear_output(wait=True)
        plt.show()
        plt.plot(range(len(loss)), loss)
        plt.show()
        time.sleep(0.5)

