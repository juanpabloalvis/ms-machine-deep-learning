from pandas_datareader import data as pdr
import numpy as np
from sklearn.model_selection import train_test_split
import scipy as sp
import fix_yahoo_finance as yf

# Manejando error
# https://github.com/VivekPa/IntroNeuralNetworks
# https://github.com/VivekPa/IntroNeuralNetworks/blob/master/get_prices.py

start_date = "2019-01-01"
end_date = "2019-04-03"
# download dataframe
data = pdr.get_data_yahoo("SPY", start=start_date, end=end_date, )
data_GOOGL = pdr.get_data_yahoo("GOOGL", start=start_date, end=end_date)
print(data.head())
# download Panel
data = pdr.get_data_yahoo(["SPY", "IWM"], start=start_date, end=end_date)
print(data.head())
