import numpy as np
np.random.seed(5) # Se debe hacer esto antes de importar la libreía de keras.
import keras
from keras.datasets import mnist
from keras.models import Sequential
from keras.layers import Dense, Dropout, Flatten
from keras.layers import Conv2D, MaxPooling2D


# https://towardsdatascience.com/build-your-own-convolution-neural-network-in-5-mins-4217c2cf964f
batch_size = 128
num_classes = 10
epochs = 12

# input image dimensions
img_rows, img_cols = 28, 28

# the data, split between train and test sets
(x_train, y_train), (x_test, y_test) = mnist.load_data()

# 60000 corresponde al número de imagenes en los datos de entrenamiento
# 28 es el tamaño en pixeles de la imagen, 1 es el número de canales(1 cuando la imagen es en
# escala de grises, cuando la imagen es a color RGB, el canal cambia a 3).
# Tambien convertimos los valores objetivos a matrices binarias
x_train = x_train.reshape(60000, 28, 28, 1)
x_test = x_test.reshape(10000, 28, 28, 1)

print('x_train shape:', x_train.shape)
print(x_train.shape[0], 'train samples')
print(x_test.shape[0], 'test samples')

# Convierte vectores de clase a vectores de clase matricial binario
y_train = keras.utils.to_categorical(y_train, num_classes)
y_test = keras.utils.to_categorical(y_test, num_classes)

# Ejemplo:
# Y = 2 # the value 2 represents that the image has digit 2
# Y = [0,0,1,0,0,0,0,0,0,0] # The 2nd position in the vector is made 1
# Here, the class value is converted into a binary class matrix


# Creamos la red (el modelo).
# Construimos un modleo secuncial y agregamos las capas convulcionadas y la capa max pooling
model = Sequential()
model.add(Conv2D(32, kernel_size=(3, 3),
                 activation='relu',
                 input_shape=(28, 28, 1)))
model.add(Conv2D(64, (3, 3), activation='relu'))
model.add(MaxPooling2D(pool_size=(2, 2)))
model.add(Dropout(0.25))
model.add(Flatten())
model.add(Dense(128, activation='relu'))
model.add(Dropout(0.5))
model.add(Dense(num_classes, activation='softmax'))

# Compilamos el modelo
model.compile(loss=keras.losses.categorical_crossentropy,
              optimizer=keras.optimizers.Adadelta(),  # Utilizamos el optimizador Adadelta
              metrics=['accuracy'])
model.fit(x_train, y_train,
          batch_size=batch_size,
          epochs=epochs,
          verbose=1,
          validation_data=(x_test, y_test))
score = model.evaluate(x_test, y_test, verbose=0)
print('Test loss:', score[0])
print('Test accuracy:', score[1])
