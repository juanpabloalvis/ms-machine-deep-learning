import pandas as pd
import matplotlib.pyplot as plt
from keras.models import Sequential
from keras.layers import Dense
from keras.callbacks import EarlyStopping

# Fuente: https://towardsdatascience.com/building-a-deep-learning-model-using-keras-1548ca149d37
# Vamos a construir un modelo de regresion para predecir el pago por hora de un empleado y a construir un modelo
# de clasificacion para validar si un paciente tiene o no diabetis

# 1) Leer (y Limpiar) los datos de entrada
train_df = pd.read_csv('data/hourly_wages_data.csv')
print(train_df.head())
# 2) Separar datos entre entradas y objetivo
# datos de entrada
train_X = train_df.drop(columns=['wage_per_hour'])
# objetivo
train_Y = train_df[['wage_per_hour']]
print(train_Y.head())

# 3) Construyendo el modelo.
# El tipo de modelo es secuencial es la forma más facil de construir un modelo en Keras, construyéndolo capa por capa.
model = Sequential()
# Obtenemos el número de columnas en los datos de entrenamiento
n_cols = train_X.shape[1]

# El tipo de capa 'Denso' es un estándar que funciona para la mayoría de casos.
# agregamos dos capas al modelo. Aunque agregamos 10, podríans ser cientos o miles. A mayor número mayor capacidad.
# La función de activación permite al modelo tener en cuenta relaciones no lineales. Esto es importante!!!.
# La funcion de activación será RecLU
model.add(Dense(10, activation='relu', input_shape=(n_cols,)))
model.add(Dense(10, activation='relu'))

# EJ: mejorando la capacidad del modelo
# model.add(Dense(200, activation='relu', input_shape=(n_cols,)))
# model.add(Dense(200, activation='relu'))
# model.add(Dense(200, activation='relu'))


# y la capa se salida
model.add(Dense(1))

# 4) Compilando el modelo.
# Toma dos parametros el optimizador y el costo.
# El optimizador controla la tasa de aprendizaje. Utilizaremos 'adam' como optimizados que es generalmente bueno
# para muchos casos. La tasa de aprendizaja determian que tan rápido se calculan los pesos optimos para el modelo.
# Una tasa pequeña pude llevara pesos pasa acertados pero el tiempo de calculo de los pesos será mas demorado.
# Para la funcion de costo, utilizaremos el error medio cuadrado. Es una funcio popular para problemas de regresión.
# Entre más cercano a cero, mejor.

model.compile(optimizer='adam', loss='mean_squared_error')

# 5) entrenando el modelo
# La validacion se llevará a cabo dividiendo aleatoriamente los datos entre entrenamiento y prueba. Durante el
# entrenamiento, podremos ver la validación de costo, que nos dá el error medio cuadrado en el conjunto de validación.
# Seleccionaremos la separacion en 0.2 que es el 20% de los datos de entrenamiento se reserban para probar el modelo.
# El número de periodos(ephoc) es el número de veces que el modelo iterará a travez de los datos. Entre mas periodos,
# el modelo mejorará hasta cierto punto, pero tardará mas tiempo. Después de cierto punto, el modelo deja de mejorar,
# Para monitorear esto, utilizaremos parada temprana(early stoping) para uqe se detenga, consideremos 3 epocas
# en el cual el costo no mejora.

early_stopping_monitor = EarlyStopping(patience=3)
model.fit(train_X, train_Y, validation_split=0.2, epochs=30, callbacks=[early_stopping_monitor])

# 6) Haciendo predicciónes sobre nuevos datos
# Hemos construido un modedlo de deep learning, en Keras.
# Sin embargo este modelo no es muy preciso, pero podemos mejorarlo con mayor cantidad de datos de entrenamiento
# y mejorando la 'capacidad del modelo'

test_df = pd.read_csv('data/test_wages_data.csv')
print("Predicciones sobre datos de prueba")
test_y_predictions = model.predict(test_df)
print(test_y_predictions)

# LA CAPACIDAD DEL MODELO
# Cuando incrementamos el número de nodos y capas en el moddelo, la capacidad incrementa y tambíen incrementa el tiempo
# como la capacidad computacional. Incrementar la capacidad  lleva a un modelo más preciso hasta cierto punto.

